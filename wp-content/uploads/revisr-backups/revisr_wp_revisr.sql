
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `wp_revisr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_revisr` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `message` text COLLATE utf8_unicode_ci,
  `event` varchar(42) COLLATE utf8_unicode_ci NOT NULL,
  `user` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `wp_revisr` WRITE;
/*!40000 ALTER TABLE `wp_revisr` DISABLE KEYS */;
INSERT INTO `wp_revisr` VALUES (1,'2019-02-19 11:23:45','Successfully created a new repository.','init','admin'),(2,'2019-02-19 11:40:19','There was an error committing the changes to the local repository.','error','admin'),(3,'2019-02-19 11:40:39','There was an error committing the changes to the local repository.','error','admin'),(4,'2019-02-19 11:41:28','Committed <a href=\"http://marianotoffi.com.ar/wp-admin/admin.php?page=revisr_view_commit&commit=2b341b3&success=true\">#2b341b3</a> to the local repository.','commit','admin'),(5,'2019-02-19 11:41:28','Error pushing changes to the remote repository.','error','admin'),(6,'2019-02-21 08:29:53','There was an error committing the changes to the local repository.','error','admin'),(7,'2019-02-21 08:29:59','Successfully backed up the database.','backup','admin'),(8,'2019-02-21 08:29:59','There was an error committing the changes to the local repository.','error','admin'),(9,'2019-02-21 08:30:19','Successfully backed up the database.','backup','admin'),(10,'2019-02-21 08:30:19','Committed <a href=\"http://marianotoffi.com.ar/wp-admin/admin.php?page=revisr_view_commit&commit=3418a31&success=true\">#3418a31</a> to the local repository.','commit','admin'),(11,'2019-02-21 08:30:20','Error pushing changes to the remote repository.','error','admin'),(12,'2019-02-21 08:30:39','Error pushing changes to the remote repository.','error','admin'),(13,'2019-02-22 22:32:33','Error pushing changes to the remote repository.','error','admin'),(14,'2019-02-22 22:36:08','Error pushing changes to the remote repository.','error','admin'),(15,'2019-02-22 22:37:34','Error pushing changes to the remote repository.','error','admin'),(16,'2019-02-26 00:33:05','Error pushing changes to the remote repository.','error','admin'),(17,'2019-02-26 00:44:53','Error pushing changes to the remote repository.','error','admin'),(18,'2019-02-26 00:46:57','Error pushing changes to the remote repository.','error','admin'),(19,'2019-02-26 00:49:45','Error pushing changes to the remote repository.','error','admin'),(20,'2019-02-26 00:50:24','Error pushing changes to the remote repository.','error','admin'),(21,'2019-02-26 00:50:32','Error pushing changes to the remote repository.','error','admin'),(22,'2019-02-26 00:51:14','Error pushing changes to the remote repository.','error','admin'),(23,'2019-02-26 00:51:42','Error pushing changes to the remote repository.','error','admin'),(24,'2019-02-26 00:53:30','Error pushing changes to the remote repository.','error','admin'),(25,'2019-02-26 00:53:49','Error pushing changes to the remote repository.','error','admin'),(26,'2019-02-26 00:56:33','Error pushing changes to the remote repository.','error','admin'),(27,'2019-02-26 01:09:18','Error pushing changes to the remote repository.','error','admin'),(28,'2019-02-26 01:14:32','Error pushing changes to the remote repository.','error','admin'),(29,'2019-02-26 01:16:48','Error pushing changes to the remote repository.','error','admin'),(30,'2019-02-26 01:18:05','Error pushing changes to the remote repository.','error','admin'),(31,'2019-02-26 01:20:56','Error pushing changes to the remote repository.','error','admin'),(32,'2019-02-26 01:26:06','Error pushing changes to the remote repository.','error','admin');
/*!40000 ALTER TABLE `wp_revisr` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

